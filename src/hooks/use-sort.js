import { useState } from 'react';

function useSort(config, data) {
  const [sortOrder, setSortorder] = useState(null);
  const [sortBy, setSortBy] = useState(null);

  const setSortColumn = (label) => {
    if (sortBy && label !== sortBy) {
      setSortorder('asc');
      setSortBy(label);
      return;
    }

    if (sortOrder === null) {
      setSortorder('asc');
      setSortBy(label);
    } else if (sortOrder === 'asc') {
      setSortorder('desc');
      setSortBy(label);
    } else if (sortOrder === 'desc') {
      setSortorder(null);
      setSortBy(label);
    }
  };

  // only sort  data if sortOrder && sortBy are not null
  // make a copy of the 'data' prop
  // find the correct sort value function and use it for sorting
  let sortedData = data;
  if (sortOrder && sortBy) {
    const { sortValue } = config.find((column) => column.label === sortBy);
    sortedData = [...data].sort((a, b) => {
      const valueA = sortValue(a);
      const valueB = sortValue(b);

      const reverseOrder = sortOrder === 'asc' ? 1 : -1;
      if (typeof valueA === 'string') {
        return valueA.localeCompare(valueB) * reverseOrder;
      } else {
        return (valueA - valueB) * reverseOrder;
      }
    });
  }

  return { sortedData, setSortColumn, sortBy, sortOrder };
}

export default useSort;
