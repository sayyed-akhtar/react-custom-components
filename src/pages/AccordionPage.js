import React from 'react';
import Accordion from '../components/Accordion';

function AccordionPage() {
  const items = [
    {
      id: 1,
      label: 'A',
      content: 'Content of A',
    },
    {
      id: 2,
      label: 'B',
      content: 'Content of B',
    },
    {
      id: 3,
      label: 'C',
      content: 'Content of C',
    },
  ];
  return <Accordion items={items} />;
}

export default AccordionPage;
