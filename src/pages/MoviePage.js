import React from 'react';
import MoviePlaylist from '../components/MoviePlaylist';
import SongPlaylist from '../components/SongPlaylist';
import Button from '../components/Button';
import { useDispatch } from 'react-redux';
import { reset } from '../store';
// import { reset, resetSongs } from '../store';
// import { reset } from '../store';

function MoviePage() {
  const dispatch = useDispatch();
  const handleResetClick = () => {
    // bad way
    // we should try to limit number of dispatches
    // dispatch(reset());
    // dispatch(resetSongs());
    //
    // Better way
    //dispatch(reset());

    // Best way
    dispatch(reset());
  };

  return (
    <div className='flex flex-col space-y-6'>
      <div className='w-max'>
        <Button danger onClick={() => handleResetClick()}>
          Reset Both Playlists
        </Button>
      </div>
      <hr />
      <MoviePlaylist />
      <hr />
      <SongPlaylist />
    </div>
  );
}

export default MoviePage;
