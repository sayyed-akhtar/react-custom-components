import { GoBell, GoCloudDownload, GoDatabase } from 'react-icons/go';
import Button from '../components/Button';

function ButtonPage() {
  return (
    <div>
      <div className='my-5'>
        <Button success rounded onClick={() => console.log('hello')}>
          <GoBell />
          Click me!!
        </Button>
      </div>
      <div className='my-5'>
        <Button danger outline>
          <GoCloudDownload />
          Buy Now!!
        </Button>
      </div>
      <div className='my-5'>
        <Button warning>
          {' '}
          <GoDatabase /> See Deals!!
        </Button>
      </div>

      <div className='my-5'>
        <Button secondary outline>
          Hide Ads!!
        </Button>
      </div>
      <div className='my-5'>
        <Button primary outline rounded>
          Cool!!
        </Button>
      </div>
    </div>
  );
}

export default ButtonPage;
