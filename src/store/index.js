import { configureStore } from '@reduxjs/toolkit';
import { moviesReducer, addMovie, removeMovie } from './slices/moviesSlice';
import { songsReducer, addSong, removeSong } from './slices/songsSlice';
import { reset } from './actions';

const store = configureStore({
  reducer: {
    songs: songsReducer,
    movies: moviesReducer,
  },
});

// export { store };
//export const { addSong, removeSong, reset: resetSongs } = songSlice.actions;
// export const { addSong, removeSong } = songSlice.actions;
// export const { addmovie, removeMovie, reset } = moviesSlice.actions;
// export const { addmovie, removeMovie } = moviesSlice.actions;

export { store, addSong, removeSong, addMovie, removeMovie, reset };
