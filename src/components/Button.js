import React from 'react';

import classNames from 'classnames';

function Button({
  children,
  primary,
  secondary,
  success,
  warning,
  danger,
  outline,
  rounded,
  ...rest
}) {
  const classes = classNames(
    rest.className,
    'px-3 py-1.5 border flex items-center',
    {
      'bg-blue-500 text-white border-blue-700': primary,
      'bg-gray-700 text-white border-gray-900': secondary,
      'bg-green-500 text-white border-green-700': success,
      'bg-yellow-500 text-white border-yellow-700': warning,
      'bg-red-500 text-white border-red-700': danger,
      'rounded-full': rounded,
      'bg-white': outline,
      'text-blue-500': outline && primary,
      'text-gray-700': outline && secondary,
      'text-green-500': outline && success,
      'text-yellow-500': outline && warning,
      'text-red-500': outline && danger,
    }
  );
  return (
    <button className={classes} {...rest}>
      {children}
    </button>
  );
}
Button.propTypes = {
  checkVariationValue: ({ primary, secondary, success, warning, danger }) => {
    const count =
      Number(!!primary) +
      Number(!!secondary) +
      Number(!!success) +
      Number(!!warning) +
      Number(!!danger);

    if (count > 1) {
      return new Error(
        'Only one of primary, secondary, success, warning, danger can be provided!'
      );
    }
  },
};
export default Button;
