import { useDispatch, useSelector } from 'react-redux';
import { createRandomSong } from '../data';
import { addSong, removeSong } from '../store';
import Button from './Button';

function SongPlaylist() {
  const dispatch = useDispatch();
  const songPlaylist = useSelector((state) => state.songs);

  const handleSongAdd = (song) => {
    console.log(song, addSong(song));
    dispatch(addSong(song));
  };
  const handleSongRemove = (song) => {
    dispatch(removeSong(song));
  };

  const renderedSongs = songPlaylist.map((song) => {
    return (
      <li
        key={song}
        className='flex justify-between items-center my-2 border-b-[1px] py-1'
      >
        {song}
        <div className='flex items-center justify-center'>
          <Button danger onClick={() => handleSongRemove(song)}>
            X
          </Button>
        </div>
      </li>
    );
  });

  return (
    <div className='content'>
      <div className='flex justify-between items-center mx-4'>
        <h3 className='text-lg font-bold'>Song Playlist</h3>
        <div className='w-max'>
          <Button primary onClick={() => handleSongAdd(createRandomSong())}>
            + Add Song to Playlist
          </Button>
        </div>
      </div>
      <ul className='m-4'>{renderedSongs}</ul>
    </div>
  );
}

export default SongPlaylist;
