import { useDispatch, useSelector } from 'react-redux';
import { createRandomMovie } from '../data';
import Button from './Button';
import { addMovie, removeMovie } from '../store';

function MoviePlaylist() {
  const dispatch = useDispatch();

  const moviePlaylist = useSelector((state) => state.movies);

  const handleMovieAdd = (movie) => {
    dispatch(addMovie(movie));
  };
  const handleMovieRemove = (movie) => {
    dispatch(removeMovie(movie));
  };

  const renderedMovies = moviePlaylist.map((movie) => {
    return (
      <li
        key={movie}
        className='flex justify-between items-center my-2 border-b-[1px] py-1'
      >
        {movie}
        <div className='flex items-center justify-center'>
          <Button onClick={() => handleMovieRemove(movie)} danger>
            X
          </Button>
        </div>
      </li>
    );
  });

  return (
    <div className='content'>
      <div className='flex justify-between items-center mx-4'>
        <h3 className='text-lg font-bold'>Movie Playlist</h3>
        <div className='w-max'>
          <Button primary onClick={() => handleMovieAdd(createRandomMovie())}>
            + Add Movie to Playlist
          </Button>
        </div>
      </div>
      <ul className='m-4'>{renderedMovies}</ul>
    </div>
  );
}

export default MoviePlaylist;
